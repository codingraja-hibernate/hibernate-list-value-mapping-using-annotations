package com.codingraja.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

@Entity
@Table(name="customer")
public class Customer {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	@Column(name="first_name")
	private String firstName;
	@Column(name="last_name")
	private String lastname;
	@Column(name="email")
	private String email;
	
	@ElementCollection
	@Column(name="mobile")
	@OrderColumn(name="mobiles", nullable=false)
	private List<Long> mobiles;
	
	public Customer(){}
	public Customer(String firstName, String lastname, String email, List<Long> mobiles) {
		super();
		this.firstName = firstName;
		this.lastname = lastname;
		this.email = email;
		this.mobiles = mobiles;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Long> getMobiles() {
		return mobiles;
	}
	public void setMobiles(List<Long> mobiles) {
		this.mobiles = mobiles;
	}
	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName + ", lastname=" + lastname + ", email=" + email
				+ ", mobiles=" + mobiles + "]";
	}
}
