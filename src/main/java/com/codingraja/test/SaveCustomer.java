package com.codingraja.test;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Customer;

public class SaveCustomer {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		List<Long> mobiles = new ArrayList<Long>();
		mobiles.add(9742900696L);
		mobiles.add(7795944511L);
		
		Customer customer = new Customer("Coding","RAJA", "info@codingraja.com", mobiles);
		
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(customer);
		transaction.commit();
		session.close();
		
		System.out.println("Customer has been saved successfully!");

	}

}
